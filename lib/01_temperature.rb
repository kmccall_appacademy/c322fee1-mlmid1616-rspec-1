def ftoc (farenheit)
  ((farenheit - 32) / (18*0.1)).ceil
end


def ctof (celsius)
  (celsius * (18*0.1)) + 32
end
