def add (num1,num2)
  num1 + num2
end

def subtract (num1,num2)
  num1 - num2
end

def sum (array)
  array.empty? ? 0 : array.reduce(:+)
end

def multiply (num1,num2)
  num1 * num2
end

def power (num,factor)
  num ** factor
end

def factorial (num)
  (1..num).to_a.reduce(:*)
end
