def echo (word)
  word
end

def shout (word)
  word.upcase
end

def repeat (word, amount=2)
  sentence = word.dup
  (amount-1).times do sentence << " " + word end
  return sentence
end

def start_of_word (word,letters_in)
  word[0..letters_in-1]
end

def first_word (sentence)
  sentence.split.first
end

def titleize (sentence)
  tiny_words = ["over","the","and"]
  sentence.split.map.with_index do |word,idx|
    if tiny_words.include?(word) && idx != 0
      word
    else
      word.capitalize
    end
  end.join(" ")
end 
