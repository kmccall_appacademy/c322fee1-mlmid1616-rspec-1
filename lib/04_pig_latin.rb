def translate (sentence)
  vowels = "aeiouAEIOU"
  pigified_sentence = []
  sentence.split.each do |word|
    not_translated = true
    word.chars.each_with_index do |letter,idx|
      if word[idx+1] && letter + word[idx+1] == "qu"
        pigified_sentence << word[idx+2..-1] + word[0...idx+2] + "ay"
        not_translated = false
      elsif vowels.include?(letter) && not_translated
        pigified_sentence << word[idx..-1] + word[0...idx] + "ay"
        not_translated = false
      end
    end
  end
  pigified_sentence.join(" ")
end
